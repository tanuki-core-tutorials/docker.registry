id: use-gitlab-docker-registry
summary: Use GitLab Docker Container Registry on GitLab.com
authors: @k33g_org

# How to use GitLab Docker Container Registry on GitLab.com
<!-- ------------------------ -->
## Introduction

We'll see how to use the [GitLab Docker Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) on GitLab.com

> Remark: there are several ways to authenticating to the GitLab Container Registry. In this tutorial, we'll use 2 special variables: **[`CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD`](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticating-to-the-container-registry-with-gitlab-cicd)**. This allows to automate building and deploying the Docker images and has read/write access to the Registry.

I will use **[Kaniko](https://github.com/GoogleContainerTools/kaniko)** to build my images.


- Author: [@k33g](https://gitlab.com/k33g)
- Source code of this tutorial: [https://gitlab.com/tanuki-core-tutorials/docker.registry](https://gitlab.com/tanuki-core-tutorials/docker.registry)

<!-- ------------------------ -->
## Requirements

You need:

- a GitLab account 
- Go to the general settings of your project to check if the **Container registry** is activated

Positive
: you can do all the steps with the GitLab Web IDE, or locally, and then push the code at the end.

<!-- ------------------------ -->
## Initialization
Duration: 5

### Use case

I write a lot of markdown documents. I store these documents on [Gitlab.com](https://gitlab.com) and I would like to use GitLab CI to check if my document are well formed. So, Il will create a Docker image with tools to help me. For this tutorial I will use only one tool today and only one image. But in "the real life", I'm using this method with various tools I need for my day to day job, like: kubectl, helm, other linters, **hey** ([an HTTP load generator](https://github.com/rakyll/hey)), ...

So I will use this pproject: [markdownlint](https://github.com/markdownlint/markdownlint). It's simple to use: `mdl README.md`

### Initialize the project

- Create a new project (mine is here: [https://gitlab.com/tanuki-core-tutorials/markdown.linter](https://gitlab.com/tanuki-core-tutorials/markdown.linter))
- Add a `Dockerfile` file
- Add a `.gitlab-ci.yml` file

<!-- ------------------------ -->
## Dockerfile
Duration: 2

```Dockerfile
FROM ubuntu:18.04
RUN apt-get update && apt-get install -y \
    ruby
RUN gem install mdl
```

Positive
: **mdl** is a ruby tool

<!-- ------------------------ -->
## .gitlab-ci.yml
Duration: 2

```yaml
stages:
  - 🚧build-img

🐳build-img:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: 🚧build-img
  script: |
    echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
```

Positive
: I use `CI_COMMIT_TAG` to to tag my imae at evey commit (btw emoji aren't mandatory 😉)


<!-- ------------------------ -->
## Build 🚀
Duration: 2

Since you commit your files, GitLab CI will build the container image

> I did the job directly on GitLab.com with the Web IDE. If you create the files locally, don't forget to push you changes 😉

Some minutes after, your image is built, 

- Go to the **Packages** menu
- Click on the **Container Registry** item
- Then, click on the docker image link

![alt step1](step1.png)

Then this is your image, you can click to get the link of the image (eg: `registry.gitlab.com/tanuki-core-tutorials/markdown.linter:latest`)

![alt step2](step2.png)

<!-- ------------------------ -->
## Use it 🐳
Duration: 2

To use your new tool, use a project with markdown files, create a stage `checking` (or use the name you want) and add this job to your `.gitlab-ci.yml` file:

```yaml
check-my-md-files:
  stage: checking
  image: registry.gitlab.com/tanuki-core-tutorials/markdown.linter:latest
  only:
    - merge_requests
  script: |
    mdl ./
```

Commit and create a **Merge Request**, let's run the CI, ... and you'll get the markdown style errors:

![alt step3](step3.png)

Positive
: 👋 the `mdl ./` command blocks the merge request, if you want to avoid the `exit 1` code, use this command: `mdl ./ || :`

🎉 That's all for today 

<!-- ------------------------ -->
## Resources

- [Building images with kaniko and GitLab CI/CD](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)



